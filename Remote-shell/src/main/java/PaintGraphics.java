package main.java;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.labels.PieSectionLabelGenerator;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.chart.renderer.category.StatisticalBarRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.statistics.DefaultStatisticalCategoryDataset;
import org.jfree.ui.RefineryUtilities;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot;  


class CstmStandardCategoryItemLabelGenerator extends StandardCategoryItemLabelGenerator {

    @Override
    public String generateLabel(CategoryDataset dataset, int row, int column) {
        return String.format("%.1f %%", dataset.getValue(row, column).doubleValue() * 100.0);
    }
}

public class PaintGraphics {

	public PaintGraphics() {
	
	}
	
	public void PaintPieChart (Container element , String percentage) {
	  
	        DefaultPieDataset dataset = new DefaultPieDataset();
	        double d = Double.parseDouble(percentage);
	        double s = 100 - d;
	        
	        PieSectionLabelGenerator labelGenerator = new  StandardPieSectionLabelGenerator(
	         "({2})" ,  new DecimalFormat("0") , new DecimalFormat("0.00%"));
	        
	        JFreeChart chart = ChartFactory.createPieChart(
	                "Uso de Disco",                                                                    
	                dataset, 
	                true, 
	                true, false
	            );
	  
	        ((PiePlot) chart.getPlot()).setLabelGenerator(labelGenerator);
	        dataset.setValue("En Uso", d);
	        dataset.setValue("Libre", s);

	        ChartPanel panel= new ChartPanel(chart);
	        
	        element.removeAll();
	        element.setPreferredSize(new Dimension(300, 300));
	        element.add(panel , BorderLayout.CENTER);
	        element.validate();
	}
	
	 public void  Bart(Container element , String memory , String swapMem) {
	   DefaultCategoryDataset dataset = new DefaultCategoryDataset();	   
	    double mem = Double.parseDouble(memory);
	    double swap = Double.parseDouble(swapMem);
	    dataset.addValue( (100 - mem)/100 , "Libre", "Memoria");
	    dataset.addValue( (mem)/100 , "En uso", "Memoria");

	   dataset.addValue((100 - swap)/100, "Libre", "Swap");
	   dataset.addValue((swap)/100, "En uso", "Swap");
	    
	      JFreeChart chart=ChartFactory.createBarChart(
	        "Uso de Memoria", //Chart Title
	        "", // Category axis
	        "", // Value axis
	        dataset,
	        PlotOrientation.VERTICAL,
	        true,true,false
	       );  

	        chart.getCategoryPlot().getRangeAxis().setLowerBound(0);
	        chart.getCategoryPlot().getRangeAxis().setUpperBound(1);
	        NumberAxis xAxis2 = (NumberAxis) chart.getCategoryPlot().getRangeAxis();
	        xAxis2.setNumberFormatOverride(NumberFormat.getPercentInstance());
 
	        CategoryPlot plot = (CategoryPlot) chart.getPlot();

	        DecimalFormat labelFormat = new DecimalFormat("##0.00 %");
	        plot.getRenderer().setSeriesItemLabelGenerator(0, new StandardCategoryItemLabelGenerator("{2}", labelFormat));
	        plot.getRenderer().setSeriesItemLabelGenerator(1, new StandardCategoryItemLabelGenerator("{2}", labelFormat));

	        plot.getRenderer().setSeriesItemLabelsVisible(0, true);
	        plot.getRenderer().setSeriesItemLabelsVisible(1, true);

	        ChartPanel panel2 = new ChartPanel(chart);

	        element.removeAll();
	        element.setPreferredSize(new Dimension(300, 300));
	        element.add(panel2 , BorderLayout.CENTER);
	        element.validate();

	    }
	 
	 public void BarProcess(Container element , String process){
		 
		 DefaultCategoryDataset dataset = new DefaultCategoryDataset();	   
		    double proc = Double.parseDouble(process);
		    
		    dataset.addValue( (proc)/100 , "En uso", "Procesador");

		    
		      JFreeChart chart=ChartFactory.createBarChart(
		        "Uso del Procesador", //Chart Title
		        "", // Category axis
		        "", // Value axis
		        dataset,
		        PlotOrientation.VERTICAL,
		        true,true,false
		       );  
		        CategoryPlot categoryPlot = chart.getCategoryPlot();
		        BarRenderer br = (BarRenderer) categoryPlot.getRenderer();
		        br.setMaximumBarWidth(.35); // set maximum width to 35% of chart;
		        
		        chart.getCategoryPlot().getRangeAxis().setLowerBound(0);
		        chart.getCategoryPlot().getRangeAxis().setUpperBound(1);
		        NumberAxis xAxis2 = (NumberAxis) chart.getCategoryPlot().getRangeAxis();
		        xAxis2.setNumberFormatOverride(NumberFormat.getPercentInstance());
		       
		        
		        CategoryPlot plot = (CategoryPlot) chart.getPlot();

		        DecimalFormat labelFormat = new DecimalFormat("##0.00 %");
		        plot.getRenderer().setSeriesItemLabelGenerator(0, new StandardCategoryItemLabelGenerator("{2}", labelFormat));
		        plot.getRenderer().setSeriesItemLabelGenerator(1, new StandardCategoryItemLabelGenerator("{2}", labelFormat));

		        plot.getRenderer().setSeriesItemLabelsVisible(0, true);
		        plot.getRenderer().setSeriesItemLabelsVisible(1, true);

		        ChartPanel panel2 = new ChartPanel(chart);

		        element.removeAll();
		        element.setPreferredSize(new Dimension(300, 300));
		        element.add(panel2 , BorderLayout.CENTER);
		        element.validate();  
	    }
}
