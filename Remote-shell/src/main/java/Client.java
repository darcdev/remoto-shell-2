/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java;

import java.io.BufferedReader;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.*;

/**
 *
 * @author darcdev
 */
public class Client {
        
     public static void main(String[] args) { 
        Socket socket;
        InetAddress IP;
        int Puerto = 2050;
        try {    
            System.out.println("Estableciendo Conexion el servidor");
            IP = InetAddress.getByName(readIp());
            socket = new Socket(IP, Puerto);
            System.out.println("Conexion establecida con el servidor");

            Register reg = new Register(socket);
            Shell sh = new Shell(socket , reg);
            Login log = new Login(socket , reg ,sh);
            log.setVisible(true);
            DataRetrieve dtr = new DataRetrieve(socket , log , sh , reg);
            dtr.start();
         }
         catch(Exception e){
    
           System.out.println(e);
    
          }   
     }
     
     public static String readIp(){
    	    
    	 File userdb = new File("ipconfig.conf");
         String ip = null;
    	 FileReader readerFile;
    	 BufferedReader bufferFile;
    	 try {
    	    readerFile = new FileReader(userdb);
    	    bufferFile = new BufferedReader(readerFile);
    		    
    	    String contenido;
    	    contenido = bufferFile.readLine();
    	    		
    	    String[] param = contenido.split("=");
    	    ip = param[1];
    		bufferFile.close();
    	}
    	catch(IOException e) {
    		System.out.println(e);	
    	 }
    	 
    	   if(!ip.equals("") || !ip.equals(null)) {
    		   return ip;
    	   }else {
    		  return "No hay ninguna ip asociada";   
    	   }
    	   
    	 }   	    
}
class DataRetrieve extends Thread {
	  Socket ss;
	  Login log;
	  Shell sh;
	  Register reg;
    
	 
	  public DataRetrieve(Socket socket , Login log ,Shell sh , Register reg) { 
		    this.ss = socket;
		    this.log = log;
		    this.sh = sh;
		    this.reg = reg;
		    
	  }
	  @Override
	  public void run() {
			try {
	            DataInput entrada;
	            String mensaje = "";
	            String mensajeEntrada;
	            String username = "";
	            do {  
	                 entrada = new DataInputStream(this.ss.getInputStream());
				     mensajeEntrada = entrada.readUTF();
				     String[] separatedMessage = mensajeEntrada.split(";");
				     String option = separatedMessage[0];
				     String response = separatedMessage[1];
				     
				     
				     if(option.equals("1") && response.equals("true")) {
				    	 String user = separatedMessage[2];
				    	 username = user;
				    	 sh.setVisible(true);
				    	 log.setVisible(false);
					     log.lblNewLabel.setText("");
	
				     }else if(option.equals("1") && !response.equals("true")) {			     
				    	 log.lblNewLabel.setText("Datos Incorrectos ,  Intentelo de nuevo");

				     }
				     else if(option.equals("2") && response.equals("true")) {
				    	 
					     reg.lblNewLabel.setText("");
				    	 reg.setVisible(false);
					  
				     }
				     else if((option.equals("2") && !response.equals("true"))) {
				         reg.lblNewLabel.setText("Usuario ya existe o Incorrecto , Intente de nuevo");
					 
				     }
				     if(option.equals("3") && !response.equals("")) {
				    	 sh.jTextArea1.setText(username + "@" + username + "-remote-shell :" + "\n\n" + response + username + "@" + username + "-remote-shell :" );
				     }
				     if(option.equals("4") && !response.equals("")) {

				    	 PaintGraphics pg  = new PaintGraphics(); 
				    	 pg.PaintPieChart(sh.jPanel10 , response);
				    	 
				     }
				     if(option.equals("5") && !response.equals("")) {
				    	 PaintGraphics pg  = new PaintGraphics();
				         String swap = separatedMessage[2];
				    	 pg.Bart(sh.jPanel11 , response , swap);
				     }
				     if(option.equals("6") && !response.equals("")) {
				    	
				    	 PaintGraphics pg  = new PaintGraphics();
				    	 pg.BarProcess(sh.jPanel9 , response);
				     }
				     Thread.sleep(30);
			     }while(mensaje.equals(""));
			}
			catch(Exception e){
				System.out.println(e);
			}
		}
	}
class DataSend extends Thread {
	Socket ss;
	String message;
	
	DataSend(Socket socket , String message){
		this.ss = socket;
		this.message =  message;
	}
	public void run() {
		DataOutput dto;
		try {
			dto = new DataOutputStream(this.ss.getOutputStream());
			dto.writeUTF(message);	
		}
		catch(Exception e) {
		}
	}	
}



