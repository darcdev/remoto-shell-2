/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;
import java.net.*;

import java.io.*;
/**
 *
 * @authors Cristian Vera , Sebastian Baez , Diego Cabrera
 */
public class Server {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        ServerSocket sServer;
        Socket sConexion = null;
        try {
            System.out.println("Creando un socket en el puerto 2050");
            sServer = new ServerSocket(2050);
            System.out.println("Esperando conexiones...");
            sConexion = sServer.accept();
            System.out.println("Se conecto un cliente...");
        } catch (Exception e) {
            System.out.println(e);
        }

        DataRetrieveServer dtr = new DataRetrieveServer(sConexion);
        dtr.start(); 

    }
}
class DataRetrieveServer extends Thread {
	Socket ss;
	public DataRetrieveServer(Socket socket) { 
	    this.ss = socket;
	}
	
	public void run() {
		try {
			DataInputStream entrada;
			String mensaje = "";
			String mensajeEntrada = "";

			do {
				entrada = new DataInputStream(ss.getInputStream());
				mensajeEntrada = entrada.readUTF();
			    if(!(mensajeEntrada.equals(null) || mensajeEntrada.equals(""))) {
			     DataSend ds =  new DataSend(this.ss, mensajeEntrada);
				 ds.start();
			    }
		 	}while(!(mensaje.equals("fin")));
		}
		catch(Exception e){
			System.out.println(e);
		}
		
	}
}
class DataSend extends Thread {
	Socket ss;
	String message;
	
	public DataSend(Socket socket , String message) { 
	    this.ss = socket;
	    this.message = message;
	}

	public void run () {
	    String mensajeSalida = "";
	    DataOutputStream salida = null;

		try { 
			
		    String[] separatedMessage = this.message.split(";");
		    String option = separatedMessage[0];
			if(option.equals("1")) {
			  String[] userData = separatedMessage[1].split(":");

			  String username = userData[0];
			  String password = userData[1];
			  Login log = new Login();
			  boolean isLogged = log.checkUserData(username, password);
			  mensajeSalida = "1;" +  Boolean.toString(isLogged) + ";" + username;
			  
			  if(isLogged) { 
			      SendMemoryPerformance smp = new SendMemoryPerformance(this.ss);
			      PerformancesendDiskSize sds = new PerformancesendDiskSize(this.ss);
			      sendProcPerformance spp = new sendProcPerformance(this.ss);
			      
			      smp.start();
			      sds.start();
			      spp.start();
			  }
			}
			else if(option.equals("2")) {
				String[] userData = separatedMessage[1].split(":");
				String username = userData[0];
				String password = userData[1];
				Register reg = new Register();
				reg.registerUser(username,password);
				mensajeSalida = "2;" + Boolean.toString(reg.verifyExistUsername(username));
				System.out.println(mensajeSalida + "Sssss");

			}
			else if(option.equals("3")) {
				String[] userData = separatedMessage[1].split(":");
				String command = userData[1];
				ProcessCommand prc = new ProcessCommand();
				mensajeSalida = "3;" + prc.ExecCommand(command);
			}
	
	        salida = new DataOutputStream (this.ss.getOutputStream()); 
		    salida.writeUTF(mensajeSalida);

		}
		catch(Exception e) {
			System.out.println(e);
		}
	}
}
class PerformancesendDiskSize extends Thread {
	Socket ss;
	public PerformancesendDiskSize(Socket socket) { 
	    this.ss = socket;
	}
	public void run () {
	
		try {
		    DataOutput salida = null;
		    String mensajeSalida;
		    PerformanceProcess ps = new PerformanceProcess();
		    
	        do {
	            mensajeSalida = "4;" + ps.diskUsage();
			    salida = new DataOutputStream (ss.getOutputStream()); 
		        salida.writeUTF(mensajeSalida);
		        Thread.sleep(500);
		    
			}while(!(mensajeSalida.equals("fin")));
		}
		catch(Exception e) {
			System.out.println(e);
		}
	}
		
}
class SendMemoryPerformance extends Thread {
	Socket ss;
	public SendMemoryPerformance(Socket socket) { 
	    this.ss = socket;
	}
	public void run () {
	
		try {
		    DataOutput salida = null;
		    String mensajeSalida;
		    PerformanceProcess ps = new PerformanceProcess();

	        do {
	             mensajeSalida = "5;" + ps.memoryUsage();
	        	salida = new DataOutputStream (ss.getOutputStream()); 
		        salida.writeUTF(mensajeSalida);
		        Thread.sleep(500);
		    
			}while(!(mensajeSalida.equals("fin")));
		}
		catch(Exception e) {
			System.out.println(e);
		}
	}
}
class sendProcPerformance extends Thread {
	Socket ss;
	public sendProcPerformance(Socket socket) { 
	    this.ss = socket;
	}
	public void run () {
	
		try {
		    DataOutput salida = null;
		    String mensajeSalida;
		    PerformanceProcess ps = new PerformanceProcess();
	        do {
	            mensajeSalida = "6;" + ps.processPerformance();
	        	salida = new DataOutputStream (ss.getOutputStream()); 
		        salida.writeUTF(mensajeSalida);
		        Thread.sleep(500);
			}while(!(mensajeSalida.equals("fin")));
		}
		catch(Exception e) {
			System.out.println(e);
		}
	}
	
}


