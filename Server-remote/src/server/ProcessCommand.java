package server;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ProcessCommand {

	
	public ProcessCommand() {
		
	}
	public String ExecCommand(String command) {
		String result = "";

		try {
			Runtime r = Runtime.getRuntime();
			Process p = r.exec(command);
			String output = "";
			BufferedReader entEstandar = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while((output = entEstandar.readLine()) != null) {				
				result = result + output + "\n";
			}
			
			return result;
		
		}
		catch(Exception e) {
			System.out.println(e);
		}
		result = "bash: comando no encontrado :"+ command + "\n\n";
		return result;
	}
	
}
