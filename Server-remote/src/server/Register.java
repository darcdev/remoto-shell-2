/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author darcdev
 */
public class Register {
        
   
    public boolean verifyExistUsername(String username){
    
        File userdb = new File("users.db");
        String userFile;
        FileReader readerFile;
        BufferedReader bufferFile;
        
        boolean existUser = false;
        
        try {
            readerFile = new FileReader(userdb);
            bufferFile = new BufferedReader(readerFile);
	    
            String contenido;
            
            while ((contenido = bufferFile.readLine()) != null) {
                String[] param = contenido.split(":");
		        userFile = param[0];
                
                if(userFile.equals(username)){
                    existUser = true;
                    break;
                }
	    }
	    bufferFile.close();
				
	}
	catch(IOException e) {
		System.out.println(e);	
        }
        return existUser;
    }
    
    public void registerUser(String username, String password) throws Exception {
        
          if(!verifyExistUsername(username)){
              
            BufferedWriter bfw;
            File userDB = new File("users.db");
            try {
            
            if(!userDB.exists()) {
                  userDB.createNewFile();
            }
            }catch (IOException ex) {
                System.out.println(ex);
            }
            try {
               
            FileWriter users = new FileWriter(userDB , true );
            bfw = new BufferedWriter(users);
            
            Cifrador cfd = new Cifrador();
            password = cfd.hash(password);
            
            bfw.write(username + ":" + password + "\n");
            bfw.close();
         
        }
        catch(IOException ex){
           System.out.println(ex);
        }
                
     } 
          
    }
    
    
}
