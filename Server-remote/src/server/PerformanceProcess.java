package server;

import java.io.*;

public class PerformanceProcess {
	
	public void PerformanceProcess(){
		
	}
	
	public String diskUsage() {
		
		String resultado = "";
		try {	
			String[] cmd = {
				"/bin/sh",
				"-c",
				"df / | awk '{usage=($4*100)/$2} END {print usage}'"
			};
			Runtime rtime = Runtime.getRuntime();
			Process proc  = rtime.exec(cmd);
			BufferedReader bfReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			String salida = "";
			while((salida = bfReader.readLine()) != null) {
				    resultado = resultado + salida;
			}
					
			}
		catch(Exception e) {
				System.out.println(e);
		}
		return resultado;
	}

	  public String memoryUsage() {
		
		String resultado = "";
		try {	
			String[] freem = {
				"/bin/sh",
				"-c",
				"free -m | grep Mem | awk '{print($3/$2)*100 \";\"}' && "
				+ "free -m | grep Swap | awk '{print($3/$2)*100 \";\"}'"
			};
			
			Runtime rtime = Runtime.getRuntime();
			Process proc = rtime.exec(freem);
			BufferedReader bfReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			String salida = "";
			while((salida = bfReader.readLine()) != null) {
				    resultado = resultado + salida;
			}
					
		}
		catch(Exception e) {
				System.out.println(e);
		}
		return resultado;
		}	
	  
	  
	  public String processPerformance() {
		  
		  String resultado = "";

		  try {	
				String[] freem = {
					"/bin/sh",
					"-c",
					"sar -u 1 1 | awk '{uso=(100-$8)} END {print uso}'"
				};
				Runtime rtime = Runtime.getRuntime();
				Process proc = rtime.exec(freem);
				BufferedReader bfReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
				String salida = "";
				while((salida = bfReader.readLine()) != null) {
					    resultado = resultado + salida;
				}
						
			}
			catch(Exception e) {
					System.out.println(e);
			}
			return resultado;
	}	
	   
}
